/**
 *  Solution to Programming Project 12.8
 *  
 *  @author Java Foundations
 */

import java.util.Scanner;
import java.util.Stack;

public class RomanProcessor {
    public static void main(String[] args) {
        Stack<String> stack = new Stack<String>();
        String romanNumeral = "";
        int decimal = 0;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter your Roman Numeral");
        romanNumeral = in.nextLine();
        in.close();

        for (int i = 0; i < romanNumeral.length(); i++) {
            String tempVar = romanNumeral.substring(i, i + 1);
            stack.push(tempVar);
        }

        int mySize = stack.size();

        for (int i = 0; i < mySize; i++) {
            String myChar = stack.peek();
            switch (myChar) {
            case "I":
                decimal = decimal + 1;
                stack.pop();
                break;

            case "V":
                decimal = decimal + 5;
                stack.pop();
                break;

            case "X":
                decimal = decimal + 10;
                stack.pop();
                break;

            case "L":
                decimal = decimal + 50;
                stack.pop();
                break;

            case "C":
                decimal = decimal + 100;
                stack.pop();
                break;

            case "D":
                decimal = decimal + 500;
                stack.pop();
                break;

            case "M":
                decimal = decimal + 1000;
                stack.pop();
                break;
            }
        }
        System.out.println("Result " + decimal);
    }
}