import java.util.Scanner;
import java.util.Stack;

/**
 *  Solution to Programming Project 12.8
 *  
 *  @author Java Foundations
 */
public class ReverseWords    
{
	/**
	 * Reads a list of strings and prints them in reverse order.
	 */
	public static void main (String[] args)
	{
		Stack<String> stack = new Stack<String>();

		try
		{
			Scanner in = new Scanner(System.in);
			System.out.println ("Enter seven words at the prompts.");
			for (int i = 1; i <= 7; i++)
			{
				System.out.print("Enter a word: ");
				stack.push(in.nextLine());
            }
            in.close();

			System.out.println("In reverse order: ");
			while (!stack.isEmpty())
				System.out.println(stack.pop());
		}
		catch (Exception IOException)
		{
			System.out.println("Input exception reported");
		}
	}
}